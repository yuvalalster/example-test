@yield('content')

@extends('layouts.app')
@section('content')

<h1> This is your task list</h1>

<table>
    <tr>
    <th>title</th>
    <th>status</th>
    <th>editing</th>
  @can('admin') <th>delete</th>@endcan
    </tr>
    @foreach($tasks as $tasks)
    <tr>
    <td>  {{$tasks->title}} </a> </td>
    <td>  {{$tasks->status}} </a> </td>
   <!-- <td>  @if ($tasks->status)-->
       <!-- <input type = 'checkbox' id ="{{$tasks->id}}" checked>-->
        <!-- <input type = 'checkbox' id ="{{$tasks->id}}" disabled='disable' checked>-->
     <!--  @else
           <input type = 'checkbox' id ="{{$tasks->id}}">
       @endif </td>-->
       <td> <a href = "{{route('tasks.edit',$tasks->id)}}"> Edit</a></td>
       <td>@can('admin') <form method = 'post' action = "{{action('TaskController@destroy', $tasks->id)}}" >

        @csrf
        @method('DELETE')

        <div class = "form-group">
    <input type = "submit" class = "form-control" name = "submit" value = "Delete">
    </div>

    </form>@endcan</td>

   <td> @if ($tasks->status == 0)
            @can('admin')
             <a href="{{route('done', $tasks->id)}}">Mark As done</a>
            @endcan 
            @else
            Done!
            @endif
       
            </td>  </tr>
   

    @endforeach
    </table>

    <a href = "{{route('tasks.create')}}"> Create a new task</a>

  <!-- @can('admin') <a href = "{{route('tasks.create')}}"> Create a new task</a>@endcan-->

    <script>
    /*
       $(document).ready(function(){
           $(":checkbox").click(function(event){
            //$(this).attr('disabled', true);
           // console.log(event.target.id)
               $.ajax({
                   url: "{{url('book5s')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type: 'put' ,
                   contentType: 'application/json',
                   data: JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}),
                   processData: false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
           });
       });
   </script>  



    <script>
    /*     $(document).ready(function(){
           $(":chekbox").click(function(event){
                console.log(event.target.id)
                $.ajax({
                   url: "{{url('books')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type: 'put' ,
                   contentType:'application/json',
                   data: JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}),
                   processData: false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
           });
       });
   </script>
     

    <style>
table, th, td {
  border: 1px solid black;
}
    </style>
@endsection