<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
//use Illuminate\Support\Facades\Response; 
use App\User;
use App\Task;
//use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;


class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function change_status($id,$status)
    {
       return view('tasks.index', ['id'=>$id, 'status'=>$status]);
    }


    public function index()
    {
        $tasks = Task::all();
        return view('tasks.index', ['tasks'=>$tasks]);
      //  $user = User::find($id);
        //$tasks = $user->tasks;
       // return view('tasks.index', compact('tasks'));
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task= new task();
        $id = Auth::id();
        //$id=1;
        $task->title = $request->title;
        //$task->user = $request->user;
        $task->status=0;
        $task->user_id=$id;
       
        $task->save();
        return redirect('tasks');
       
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::find($id);
        return view('tasks.edit',compact('task'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Task::find($id);
       // if(!$task->user->id == Auth::id()) return(redirect('tasks'));
        $task->update($request->except(['_token']));

       // if($request->ajax()){
           // return Response::json(array('result'=>'success', 'status'=>$request->status),200);
     //   }

        return redirect('tasks');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if (Gate::denies('admin')) {
            abort(403,"Are you a hacker or what?");
       }
        $task = Task::find($id);
        $task-> delete();
        return redirect('tasks');

        
    }

    public function done($id)
    {
        //only if this todo belongs to user 
        if (Gate::denies('admin')) {
            abort(403,"You are not allowed to mark tasks as dome..");
         }          
        $task = Task::findOrFail($id);            
        $task->status = 1; 
        $task->save();
        return redirect('tasks');    
    } 
}
